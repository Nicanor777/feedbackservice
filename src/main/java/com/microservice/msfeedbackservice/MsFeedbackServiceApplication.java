package com.microservice.msfeedbackservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsFeedbackServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsFeedbackServiceApplication.class, args);
    }

}

