package com.microservice.msfeedbackservice.datalayer.repositories;

import com.microservice.msfeedbackservice.services.FeedbackService;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FeedbackServiceRepository extends JpaRepository<FeedbackService, Long> {
    List<FeedbackService> findByMsisdn(String msisdn);
    Optional<FeedbackService> findById(Long id);

}