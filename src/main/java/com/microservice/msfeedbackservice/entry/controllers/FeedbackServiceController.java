package com.microservice.msfeedbackservice.entry.controllers;

import com.microservice.msfeedbackservice.services.FeedbackService;
import com.microservice.msfeedbackservice.utils.GlobalVariables;
import com.microservice.msfeedbackservice.datalayer.repositories.FeedbackServiceRepository;
import com.microservice.msfeedbackservice.models.FeedbackServicesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.*;

@RestController
@RequestMapping("api/v1")
public class FeedbackServiceController {
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long RefById;

    @Autowired
    FeedbackServiceRepository feedbackServiceRepository;

    @GetMapping("/feedback")
    public ResponseEntity<Object> getAllFeedbackService(HttpServletRequest request, @RequestParam(required = false) String msisdn){
        /** String requestKey = request.getHeader("X_MSISDN");
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set(GlobalVariables.REQUEST_REFERENCE_ID, String.valueOf(RefById));
        responseHeaders.set(String.valueOf(GlobalVariables.RESPONSE_CODE), "200");
        responseHeaders.set(GlobalVariables.RESPONSE_MESSAGE, "REQUEST SUCCESSFUL");
        responseHeaders.set(GlobalVariables.CUSTOMER_MESSAGE, "REQUEST SUCCESSFUL");
        responseHeaders.set(GlobalVariables.TIME_STAMP, String.valueOf(Timestamp.from(Instant.now())));
        responseHeaders.set(GlobalVariables.MSISDN, requestKey); **/

        Map<String, Object> header = new HashMap<>();
        header.put(GlobalVariables.REQUEST_REFERENCE_ID, RefById);
        header.put(GlobalVariables.RESPONSE_CODE, "200");
        header.put(GlobalVariables.RESPONSE_MESSAGE, "REQUEST SUCCESSFUL");
        header.put(GlobalVariables.CUSTOMER_MESSAGE, "REQUEST SUCCESSFUL");
        header.put(GlobalVariables.TIME_STAMP, String.valueOf(Timestamp.from(Instant.now())));

        FeedbackServicesResponse response = new FeedbackServicesResponse(header);

        try {
            List<FeedbackService> feedbackServices = new ArrayList<>();

            if (msisdn == null)
                feedbackServices.addAll(feedbackServiceRepository.findAll());
            else feedbackServices.addAll(feedbackServiceRepository.findByMsisdn(msisdn));

            if (feedbackServices.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            response.setContent(feedbackServices);
            return ResponseEntity.ok(response);

            //return new ResponseEntity<>(feedbackServices, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/feedback/{id}")
    public ResponseEntity<Object> getFeedbackServiceById(HttpServletRequest request, @PathVariable("id") Long id){
        Map<String, Object> idHeader = new HashMap<>();
        idHeader.put(GlobalVariables.REQUEST_REFERENCE_ID, RefById);
        idHeader.put(GlobalVariables.RESPONSE_CODE, "200");
        idHeader.put(GlobalVariables.RESPONSE_MESSAGE, "REQUEST SUCCESSFUL");
        idHeader.put(GlobalVariables.CUSTOMER_MESSAGE, "REQUEST SUCCESSFUL");
        idHeader.put(GlobalVariables.TIME_STAMP, String.valueOf(Timestamp.from(Instant.now())));

        FeedbackServicesResponse responseById = new FeedbackServicesResponse(idHeader);

        /** try {
            List<FeedbackService> feedbackServicesById = new ArrayList<>();

            if (id == null) {
                feedbackServicesById.addAll(feedbackServiceRepository.findAll());
            } else {
                feedbackServicesById.addAll(feedbackServiceRepository.findAll());
            }
            if (feedbackServicesById.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

            responseById.setContent(feedbackServicesById);
            return ResponseEntity.ok(request);

        }catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        } **/

        Optional<FeedbackService> feedbackServiceData = feedbackServiceRepository.findById(id);

        if (feedbackServiceData.isPresent()){
            return new ResponseEntity<>(feedbackServiceData.get(), HttpStatus.OK);
            //responseById.setContent(feedbackServiceData);
            //return ResponseEntity.ok(responseById);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/feedback")
    public ResponseEntity<FeedbackService> createFeedbackService(@RequestBody FeedbackService feedbackService){
        try {
            FeedbackService _feedbackservice = feedbackServiceRepository
                    .save(new FeedbackService(feedbackService.getFirstName(), feedbackService.getSecondName(),feedbackService.getMsisdn()));
            return new ResponseEntity<>(_feedbackservice, HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/feedback/{id}")
    public ResponseEntity<FeedbackService> updateFeedbackService(@PathVariable("id") long id, @RequestBody FeedbackService feedbackService){
        Optional<FeedbackService> feedbackServiceData = feedbackServiceRepository.findById(id);

        if (feedbackServiceData.isPresent()){
            FeedbackService _feedbackservice = feedbackServiceData.get();
            _feedbackservice.setFeedbackMessage(feedbackService.getFeedbackMessage());
            return new ResponseEntity<>(feedbackServiceRepository.save(_feedbackservice), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}