package com.microservice.msfeedbackservice.models;

import com.microservice.msfeedbackservice.services.FeedbackService;

import java.util.List;
import java.util.Map;

public class FeedbackServicesResponse {
    Map<String, Object> header;

    Body body;

    public FeedbackServicesResponse(Map<String, Object> header) {
        this.header = header;

        body = new Body();
    }

    public Body getBody() {
        return body;
    }

    public void setContent(List<FeedbackService> content) {
        this.body.content = content;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public class Body {
        List<FeedbackService> content;

        public List<FeedbackService> getContent() {
            return content;
        }

        public void setContent(List<FeedbackService> content) {
            this.content = content;
        }
    }

    public Map<String, Object> getHeader() {
        return header;
    }

    public void setHeader(Map<String, Object> header) {
        this.header = header;
    }
}
