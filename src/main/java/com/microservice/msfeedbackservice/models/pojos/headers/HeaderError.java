package com.microservice.msfeedbackservice.models.pojos.headers;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HeaderError {
    @JsonProperty("header")
    private String header;

    @JsonProperty("error")
    private String error;

    public HeaderError() {}

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return String.format("{" +
                "\"header\": \"%s\"" +
                "\"error\": \"%s\"" +
                "}", header, error);
    }
}
