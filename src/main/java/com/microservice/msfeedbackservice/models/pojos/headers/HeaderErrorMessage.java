package com.microservice.msfeedbackservice.models.pojos.headers;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HeaderErrorMessage implements Serializable {
    @JsonProperty("missingHeaders")
    private boolean missingHeaders;

    @JsonProperty("invalidHeaders")
    private List<HeaderError> invalidHeaderErrors;

    public HeaderErrorMessage(){
        invalidHeaderErrors = new ArrayList<>();
    }

    public boolean isMissingHeaders() {
        return missingHeaders;
    }

    public void setMissingHeaders(boolean missingHeaders) {
        this.missingHeaders = missingHeaders;
    }

    public List<HeaderError> getInvalidHeaderErrors() {
        return invalidHeaderErrors;
    }

    public void setInvalidHeaderErrors(List<HeaderError> invalidHeaderErrors) {
        this.invalidHeaderErrors = invalidHeaderErrors;
    }

    @Override
    public String toString() {
        return String.format("{" +
                "\"invalidHeaderErrors\": \"%s\", " +
                "\"invalid\": [%s] " +
                "}", missingHeaders, invalidHeaderErrors);
    }
}
