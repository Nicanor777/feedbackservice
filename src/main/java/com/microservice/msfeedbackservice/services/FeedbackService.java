package com.microservice.msfeedbackservice.services;

import javax.persistence.*;

@Entity
@Table(name = "feedback")
public class FeedbackService {

    public FeedbackService(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "firstName")
    private String firstName;

    @Column(name = "secondName")
    private String secondName;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name =  "FeedbackMessage")
    private String feedbackMessage;


    public FeedbackService(String firstName, String secondName, String msisdn){
        this.firstName = firstName;
        this.secondName = secondName;
        this.msisdn = msisdn;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getFeedbackMessage() {
        return feedbackMessage;
    }

    public void setFeedbackMessage(String feedbackMessage) {
        this.feedbackMessage = feedbackMessage;
    }

}

