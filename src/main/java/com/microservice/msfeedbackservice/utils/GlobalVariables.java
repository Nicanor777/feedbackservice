package com.microservice.msfeedbackservice.utils;

public class GlobalVariables {
    public static final String REQUEST_REFERENCE_ID = "requestRefId";
    public static final String RESPONSE_MESSAGE = "responseMessage";

    public static final String LOGGER_FORMAT = "TransactionID={} | TransactionType={} | Process={} | ProcessDuration={} | Msisdn={} | SourceSystem={} | TargetSystem={}  | response={} |" +
            "ResponseCode={}  | ResponseMsg={} | ErrorDescription={} | RequestPayload={} | ResponsePayload={}";
    public static final String MSISDN = "Msisdn";
    public static final String RESPONSE_CODE = "200";
    public static final String CUSTOMER_MESSAGE = "customerMessage";
    public static final String TIME_STAMP = "timeStamp";
}
