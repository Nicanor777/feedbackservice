package com.microservice.msfeedbackservice.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogsManager {
    private static final Logger logger = LoggerFactory.getLogger(LogsManager.class);

    private LogsManager(){
    }

    public static void warn(String requestId, String msisdn, String firstName, String sourceSystem, String errorDscription, String responseCode, String response){
        logger.warn(GlobalVariables.LOGGER_FORMAT, requestId, msisdn, firstName, sourceSystem, errorDscription, responseCode, response);
    }

    public static void info(String requestId, String msisdn, String firstName){
        logger.info(GlobalVariables.LOGGER_FORMAT, requestId, msisdn, firstName);
    }

    public static void error(String requestId, String msisdn, String firstName){
        logger.error(GlobalVariables.LOGGER_FORMAT, requestId, msisdn, firstName);
    }

    /** public static void info(String requestId, String msisdn, String firstName, String secondName, String feedbackMessage){
        String loggerFormat = LOGGER_FORMAT + "| RequestHeaders={}";
        logger.info(loggerFormat, requestId, msisdn, firstName, secondName, feedbackMessage);
    } **/
}
